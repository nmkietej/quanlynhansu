import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/systems/Layout';
import { Home } from './components/features/Home';
import { FetchData } from './components/features/FetchData/FetchData';
import { Counter } from './components/features/Counter';
import { department } from './components/features/DepartmentManerger/department';

export default class App extends Component {
    static displayName = App.name;

    render() {
        return (
            <Layout>
                < Route exact path='/' component={Home} />
                < Route path='/counter' component={Counter} />
                < Route path='/fetch-data' component={FetchData} />
                < Route path='/department-data' component={department} />
            </Layout >
        );
    }
}