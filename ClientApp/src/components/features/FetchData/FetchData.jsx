import React, { Component } from 'react';
import Forecasts from './model/Forecasts';

export class FetchData extends Component {
  static displayName = FetchData.name;

  constructor(props) {
    super(props);
    this.state = { forecasts: null, loading: true };
  }

  componentDidMount() {
    this.setState({ forecasts: this.loadData(), loading: false });
  }

  loadData() {
    var data = [];
    for (var i = 0; i < 10; ++i) {
      var item = new Forecasts();
      item.id = i;
      var today = new Date();
      item.dateFormatted = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+ (today.getDate() + i);
      item.temperatureC = i + 1;
      item.temperatureF = i + 10;
      item.summary = (item.temperatureC + item.temperatureF) / 2;
      data.push(item);
    }
    console.log(data);
    return data;
  }

  static renderForecastsTable(forecasts) {
    return (
      <table className='table table-striped'>
        <thead>
          <tr>
            <th>Date</th>
            <th>Temp. (C)</th>
            <th>Temp. (F)</th>
            <th>Summary</th>
          </tr>
        </thead>
        <tbody>
          {forecasts.map(forecast =>
            <tr key={forecast.id}>
              <td>{forecast.dateFormatted}</td>
              <td>{forecast.temperatureC}</td>
              <td>{forecast.temperatureF}</td>
              <td>{forecast.summary}</td>
            </tr>
          )}
        </tbody>
      </table>
    );
  }

  render() {
    let contents = this.state.loading
      ? <p><em>Loading...</em></p>
      : FetchData.renderForecastsTable(this.state.forecasts);

    return (
      <div>
        <h1>Weather forecast</h1>
        <p>This component demonstrates fetching data from the server.</p>
        {contents}
      </div>
    );
  }
}
